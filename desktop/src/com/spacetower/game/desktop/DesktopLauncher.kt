package com.spacetower.game.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.utils.viewport.FitViewport
import com.spacetower.game.SpaceTowerGame

fun main(arg: Array<String>) {
    DesktopLauncher.run()
}

object DesktopLauncher {
    fun run() {
        SpaceTowerGame.viewportProvider = { camera ->
            FitViewport(1024f, 720f, camera)
        }

        val config = LwjglApplicationConfiguration()
        LwjglApplication(SpaceTowerGame, config)
    }
}
