package com.spacetower.game

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.spacetower.game.render.GameScreen
import com.spacetower.game.util.TimedTask

object SpaceTowerGame : Game() {
    // Constants
    val CLEAR_COLOR = Color(Color.BLACK)

    // Dependency Injection - Set these before calling create()
    var viewportProvider: (Camera) -> Viewport = { cam -> ScreenViewport() }


    // Private Properties
    private val physicsActivity = TimedTask(1f / 60f, { deltaT -> updatePhysics(deltaT) })


    fun tick(deltaT: Float) {

    }

    override fun create() {
        this.setScreen(GameScreen)
    }

    override fun render() {
        super.render()
    }

    override fun dispose() {
        GameScreen.dispose()
    }


    private fun updatePhysics(deltaT: Float) {

    }

}
