package com.spacetower.game.render

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.viewport.FitViewport
import com.spacetower.game.SpaceTowerGame

/**
 * Copyright (c) 2016 Nathan Templon
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
object GameScreen: Screen {

    private val img by lazy { Texture("badlogic.jpg") }
    private val batch by lazy { SpriteBatch() }
    private val camera = OrthographicCamera()
    private val viewport = FitViewport(1024f, 720f, camera)

    override fun show() {

    }

    override fun pause() {

    }

    override fun resize(width: Int, height: Int) {
        this.viewport.setWorldSize(width.toFloat(), height.toFloat())
        this.viewport.update(width, height)
    }

    override fun hide() {

    }

    override fun render(delta: Float) {
        SpaceTowerGame.tick(delta)

        this.camera.update()
        this.batch.projectionMatrix = camera.combined

        Gdx.gl.glClearColor(SpaceTowerGame.CLEAR_COLOR.r, SpaceTowerGame.CLEAR_COLOR.g, SpaceTowerGame.CLEAR_COLOR.b, SpaceTowerGame.CLEAR_COLOR.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        batch.begin()
        batch.draw(img, 0f, 0f)
        batch.end()
    }

    override fun resume() {

    }

    override fun dispose() {
        this.img.dispose()
        this.batch.dispose()
    }

}